import React from "react";
import LoadRoutes from "./routes";
import Container from "@material-ui/core/Container";

function App() {
  return (
    <Container maxWidth="sm" style={{ marginTop: 100 }}>
      <LoadRoutes />
    </Container>
  );
}

export default App;
