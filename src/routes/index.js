import React from "react";
import { Switch, Route } from "react-router-dom";
import Login from "../containers/Login/Login";
import Register from "../containers/Register/Register";
import Posts from "../containers/Posts/Posts";
import NewPost from "../containers/Posts/NewPost";
import PrivateRoute from "./PrivateRoute";
import Profile from "../containers/Profile/Profile";
import SearchResults from "../containers/SearchResults/SearchResults";

const LoadRoutes = () => {
  return (
    <Switch>
      <Route path="/login" exact component={Login} />
      <Route path="/register" exact component={Register} />
      <PrivateRoute path="/" exact>
        <Posts />
      </PrivateRoute>
      <PrivateRoute path="/new-post" exact>
        <NewPost />
      </PrivateRoute>
      <PrivateRoute path="/profile">
        <Profile />
      </PrivateRoute>
      <PrivateRoute path="/search">
        <SearchResults />
      </PrivateRoute>
    </Switch>
  );
};

export default LoadRoutes;
