import React from "react";
import { Route, Redirect } from "react-router-dom";

function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        JSON.parse(localStorage.getItem("isAuthenticated")) ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { form: location }
            }}
          />
        )
      }
    />
  );
}

export default PrivateRoute;
