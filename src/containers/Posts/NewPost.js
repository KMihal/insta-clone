import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { TextField, Button, Box } from "@material-ui/core";
import moment from "moment";
import { newPost } from "../../api/posts";
import { withRouter } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  },
  input: {
    display: "none"
  },
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "100%"
  }
}));

const NewPost = ({ history }) => {
  const [description, setDescription] = useState("");
  const [file, setFile] = useState();

  const classes = useStyles();

  const handleSubmit = e => {
    let data = {
      description,
      picture: file,
      createdDate: moment(),
      user: window.currentUser
    };

    newPost(data)
      .then(response => {
        if (response.status === 200) {
          history.push("/");
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  const handleDescriptionChange = e => {
    setDescription(e.target.value);
  };

  const handleFileChange = e => {
    setFile(e.target.files[0]);
  };

  return (
    <Paper className={classes.root}>
      <form className={classes.container} encType="multipart/form-data">
        <TextField
          name="description"
          id="standard-basic"
          className={classes.textField}
          label="Desciption"
          fullWidth
          margin="normal"
          onChange={e => handleDescriptionChange(e)}
        />
        <input
          accept="image/*"
          className={classes.input}
          id="contained-button-file"
          multiple
          type="file"
          name="description"
          onChange={e => handleFileChange(e)}
        />
        <label htmlFor="contained-button-file">
          <Button
            variant="contained"
            color="primary"
            component="span"
            margin="normal"
          >
            Upload
          </Button>
        </label>
        <Box alignContent="flex-end">
          <Button
            onClick={e => {
              handleSubmit(e);
            }}
          >
            Create post
          </Button>
        </Box>
      </form>
    </Paper>
  );
};

export default withRouter(NewPost);
