import React, { useState, useEffect, useRef } from "react";
import { getPosts, deletePost } from "../../api/posts";
import { apiRoot } from "../../api/api";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import { red } from "@material-ui/core/colors";
import moment from "moment";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  card: {
    maxWidth: "100%"
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  }
}));

const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [deleted, setDeleted] = useState(false);
  const classes = useStyles();

  let currentUserId = useRef(window.currentUser);

  const handlePostDelete = postId => {
    deletePost(postId).then(response => {
      const deletedPostId = response.data.response._id;

      const updatedPosts = posts;

      for (let i in updatedPosts) {
        if (updatedPosts[i]._id === deletedPostId) {
          delete updatedPosts[i];
        }
      }

      setDeleted(true);
    });
  };

  useEffect(() => {
    getPosts()
      .then(response => {
        let receivedPosts = response.data.posts;

        let sortedPosts = receivedPosts
          .sort((a, b) => {
            return (
              new Date(a.createdDate).getTime() -
              new Date(b.createdDate).getTime()
            );
          })
          .reverse();

        setPosts(sortedPosts);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  useEffect(() => {
    getPosts()
      .then(response => {
        let receivedPosts = response.data.posts;

        let sortedPosts = receivedPosts
          .sort((a, b) => {
            return (
              new Date(a.createdDate).getTime() -
              new Date(b.createdDate).getTime()
            );
          })
          .reverse();

        setPosts(sortedPosts);
        setDeleted(false);
      })
      .catch(error => {
        console.error(error);
      });
  }, [deleted]);

  const loadPicture = post => {
    if (post.picture !== undefined) {
      return (
        <CardMedia
          className={classes.media}
          image={apiRoot + "/" + post.picture}
        />
      );
    }
  };

  return (
    <Grid container spacing={2}>
      {posts.map(post => (
        <Grid item xs={12} key={post._id.toString()}>
          <Card className={classes.card}>
            <CardHeader
              avatar={
                <Avatar className={classes.avatar}>
                  {post.user ? post.user.firstName[0] : "A"}
                </Avatar>
              }
              action={
                post.user !== null && currentUserId._id === post.user._id ? (
                  <Button
                    color="secondary"
                    onClick={() => {
                      handlePostDelete(post._id);
                    }}
                  >
                    Delete
                  </Button>
                ) : null
              }
              title={post.description}
              subheader={moment(post.createdDate).format("MMMM DD YYYY")}
            />
            {loadPicture(post)}
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default Posts;
