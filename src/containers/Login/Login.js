import React, { useState } from "react";
import { login } from "../../api/authentication";
import Paper from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const Login = ({ history }) => {
  const classes = useStyles();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const submit = e => {
    e.preventDefault();

    const credentials = {
      username: username,
      password: password
    };

    login(credentials)
      .then(response => {
        localStorage.setItem("userInfo", JSON.stringify(response.data));
        localStorage.setItem("isAuthenticated", JSON.stringify(true));
        history.push("/");
      })
      .catch(error => {
        console.error(error);
      });
  };

  const onChangeUsername = e => {
    setUsername(e.target.value);
  };

  const onChangePassword = e => {
    setPassword(e.target.value);
  };

  return (
    <Paper className={classes.root}>
      <form>
        <Grid container>
          <Grid item xs={12}>
            <TextField
              name="username"
              onChange={e => {
                onChangeUsername(e);
              }}
              placeholder="Username"
              fullWidth
              margin="normal"
            />
          </Grid>

          <Grid item xs={12}>
            <TextField
              name="password"
              onChange={e => {
                onChangePassword(e);
              }}
              placeholder="Password"
              type="password"
              fullWidth
              margin="normal"
            />
          </Grid>
          <Grid item xs={6}>
            <Box display="flex" justifyContent="flex-start">
              <Button variant="contained" color="primary" onClick={submit}>
                Login
              </Button>
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box
              display="flex"
              justifyContent="flex-end"
              alignItems="flex-start"
            >
              <Button
                color="secondary"
                onClick={() => {
                  history.push("/register");
                }}
              >
                Register
              </Button>
            </Box>
          </Grid>
        </Grid>
      </form>
    </Paper>
  );
};

export default Login;
