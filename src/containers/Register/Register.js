import React, { useState } from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { registerUser } from "../../api/user";
import { withRouter } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  },
  input: {
    display: "none"
  },
}));

const Register = ({ history }) => {
  const [username, setUsername] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [telephone, setTelephone] = useState("");
  const [password, setPassword] = useState("");
  const [ file, setFile ] = useState("");

  const classes = useStyles();

  const handleOnChange = e => {
    if (e.target.name === "firstName") {
      setFirstName(e.target.value);
    } else if (e.target.name === "lastName") {
      setLastName(e.target.value);
    } else if (e.target.name === "email") {
      setEmail(e.target.value);
    } else if (e.target.name === "password") {
      setPassword(e.target.value);
    } else if (e.target.name === "tel") {
      setTelephone(e.target.value);
    } else {
      setUsername(e.target.value);
    }
  };

  const handleRegister = () => {
    const userData = {
      username,
      firstName,
      lastName,
      email,
      telephone,
      password,
      picture: file
    };

    registerUser(userData)
      .then(response => {
        history.push("/login");
      })
      .catch(error => {
        console.error(error);
      });
  };

  const handleFileChange = (e) => {
      setFile(e.target.files[0]);
  }

  return (
    <Paper className={classes.root}>
      <form>
        <Grid container>
          <Grid item xs={12}>
            <TextField
              name="username"
              placeholder="Username"
              fullWidth
              margin="normal"
              onChange={e => {
                handleOnChange(e);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="firstName"
              placeholder="First name"
              fullWidth
              margin="normal"
              onChange={e => {
                handleOnChange(e);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="lastName"
              placeholder="Last name"
              fullWidth
              margin="normal"
              onChange={e => {
                handleOnChange(e);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="email"
              placeholder="Email"
              fullWidth
              margin="normal"
              onChange={e => {
                handleOnChange(e);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="tel"
              placeholder="Telephone"
              fullWidth
              margin="normal"
              onChange={e => {
                handleOnChange(e);
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="password"
              placeholder="Password"
              type="password"
              fullWidth
              margin="normal"
              onChange={e => {
                handleOnChange(e);
              }}
            />
          </Grid>
          <Grid item xs={12}>
              <input
              accept="image/*"
              className={classes.input}
              id="contained-button-file"
              multiple
              type="file"
              name="description"
              onChange={e => handleFileChange(e)}
            />
            <label htmlFor="contained-button-file">
              <Button
                variant="contained"
                color="primary"
                component="span"
                margin="normal"
              >
                Upload
              </Button>
            </label>
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleRegister}
            >
              Register
            </Button>
          </Grid>
        </Grid>
      </form>
    </Paper>
  );
};

export default withRouter(Register);
