import React, {useState, useEffect} from 'react';
import queryString from 'query-string';
import { withRouter } from 'react-router-dom';
import { getUsers } from '../../api/user';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3,2)
    }
}))

const SearchResults = ({ location }) => {
    const qs = queryString.parse(location.search)

    const [users, setUsers] = useState([])

    useEffect(() => {
        setUsers([])
        getUsers().then(response => {
            let foundUsers = response.data.users.filter(item => {
                return item.username.toLowerCase().indexOf(qs.q) >= 0
            })

            setUsers(foundUsers)
        }).catch(error => {
            console.error(error)
        })
    }, [qs.q])

    const classes = useStyles();
    
    return (
        <Grid container spacing={3}>
            {
                users.map(user => (
                    <Grid item xs={12} key={user._id.toString()}>
                        <Paper className={classes.root}>
                            <Typography variant="h6">{user.username}</Typography>
                            <Typography variant="caption">{user.email}</Typography>
                        </Paper>
                    </Grid>
                ))
            }
                        
        </Grid>
    )
}

export default withRouter(SearchResults);