import React, { useEffect, useState, useRef } from "react";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import Avatar from "@material-ui/core/Avatar";
import { red } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";
import { getPosts, deletePost } from "../../api/posts";
import { apiRoot } from "../../api/api";
import moment from "moment";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  },
  card: {
    maxWidth: "100%"
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  }
}));

const Profile = () => {
  const classes = useStyles();
  const user = useRef(window.currentUser);

  const [posts, setPosts] = useState([]);
  const [deleted, setDeleted] = useState([]);

  useEffect(() => {
    getPosts()
      .then(response => {
        if (response.status === 200) {
          let posts = response.data.posts;

          let userPosts = posts.filter(post => {
            return post.user !== null && user.current.user._id === post.user._id;
          });

          setPosts(userPosts);
        }
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  useEffect(() => {
    getPosts()
      .then(response => {
        let posts = response.data.posts;

        let userPosts = posts.filter(post => {
          return post.user !== null && user.current.user._id === post.user._id;
        });

        setPosts(userPosts);
        setDeleted(false);
      })
      .catch(error => {
        console.error(error);
      });
  }, [deleted]);

  console.log(user.current.user);

  const loadPicture = post => {
    if (post.picture !== undefined) {
      return (
        <CardMedia
          className={classes.media}
          image={apiRoot + "/" + post.picture}
        />
      );
    }
  };

  const handlePostDelete = postId => {
    deletePost(postId).then(response => {
      const deletedPostId = response.data.response._id;

      const updatedPosts = posts;

      for (let i in updatedPosts) {
        if (updatedPosts[i]._id === deletedPostId) {
          delete updatedPosts[i];
        }
      }

      setDeleted(true);
    });
  };

  return (
    <Grid container spacing={3} justify="center" alignItems="center">
      <Grid item xs={12}>
        <Paper className={classes.root}>
          <Grid container>
            <Grid item xs={12}>
              <Card>
                {user.current.user.picture === undefined ? null : (
                  <CardMedia
                    className={classes.media}
                    image={apiRoot + "/" + user.user.picture}
                  />
                )}

                <CardContent>
                  <Typography variant="h5">{`${user.current.user.firstName} ${user.current.user.lastName}`}</Typography>
                  <Typography variant="caption">{user.current.user.email}</Typography>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        {posts.length > 0 ? (
          posts.map(post => (
            <Card width={1} key={post._id.toString()} className={classes.card}>
              <CardHeader
                avatar={
                  <Avatar className={classes.avatar}>
                    {user.current.user.firstName[0]}
                  </Avatar>
                }
                action={
                  <Button
                    onClick={() => {
                      handlePostDelete(post._id);
                    }}
                    color="secondary"
                  >
                    Delete
                  </Button>
                }
                title={post.description}
                subheader={moment(post.createdDate).format("MMMM DD YYYY")}
              />
              {loadPicture(post)}
            </Card>
          ))
        ) : (
          <Paper className={classes.root}>
            <Typography variant="body2">
              No posts found for this user
            </Typography>
          </Paper>
        )}
      </Grid>
    </Grid>
  );
};

export default Profile;
