import React from "react";
import AppBar from "@material-ui/core/AppBar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { Toolbar, Typography, IconButton } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import PersonIcon from "@material-ui/icons/Person";
import { withRouter } from "react-router-dom";
import { logout } from "../../api/authentication";
import Search from '../Search/Search'
import { Link } from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  },
  link: {
    textDecoration: "none",
    color: "#fff"
  }
}));

const NavigationBar = ({ history }) => {
  const classes = useStyles();

  const handleCreatePost = () => {
    history.push("/new-post");
  };

  const handleLogout = () => {
    logout();
    history.push("/login");
  };

  const handleProfile = () => {
    history.push("/profile");
  };

  return (
    <div className={classes.grow}>
    <AppBar>
      <Toolbar>
        <Typography variant="h6"><Link className={classes.link} to="/">Instagram clone</Link></Typography>
        {JSON.parse(localStorage.getItem("isAuthenticated")) ? (
        <Search />
        ) : null}
        <div className={classes.grow} />
        {JSON.parse(localStorage.getItem("isAuthenticated")) ? (
          <div className={classes.sectionDesktop}>
            <IconButton onClick={handleCreatePost}>
              <AddCircleIcon />
            </IconButton>
            <IconButton onClick={handleProfile}>
              <PersonIcon />
            </IconButton>
            <IconButton onClick={handleLogout}>
              <ExitToAppIcon />
            </IconButton>
          </div>
        ) : null}
      </Toolbar>
    </AppBar>
    </div>
  );
};

export default withRouter(NavigationBar);
