import React, { useState } from 'react';
import {fade, makeStyles} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    grow: {
        flexGrow: 1,
      },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(3),
          width: 'auto',
        },
      },
      searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      inputRoot: {
        color: 'inherit',
      },
      inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: 200,
        },
      },
}))

const Search = ({ history }) => {

    const classes = useStyles();

    const [querySearch, setQuerySearch] = useState()

    const onEnterPress = (e) => {
        if(e.key === 'Enter') {
            history.push(`search?q=${querySearch}`)
        }
    }


    return(
        <div className={classes.grow}>
            <div className={classes.search}>
                <div className={classes.searchIcon}>
                    <SearchIcon />
                </div>
                <InputBase 
                    placeholder="search"
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput
                    }}
                    onChange={(e) => { setQuerySearch(e.target.value) }}
                    inputProps={{ 'aria-label': 'search' }}
                    onKeyDown={ (e) => { onEnterPress(e) } }
                />
            </div>
        </div>
    )
}

export default withRouter(Search);
