import { apiRoot } from "./api";
import axios from "axios";
import { appendFormData } from "./config";

export const login = credentials => {
  let formData = appendFormData(credentials);
  return axios.post(`${apiRoot}/users/login`, formData);
};

export const getUserInfo = () => {
  return JSON.parse(localStorage.getItem("userInfo"));
};

export const getAuthHeader = () => {
  if (getUserInfo()) {
    return { Authorization: `Bearer ${getUserInfo().token}` };
  }
};

export const logout = () => {
  localStorage.removeItem("userInfo");
  localStorage.removeItem("isAuthenticated");
};
