import { apiRoot } from "./api";
import { appendFormData } from "./config";
import axios from "axios";

export const getUsers = () => {
  return axios.get(`${apiRoot}/users`);
};

export const registerUser = userData => {
  let formData = appendFormData(userData);
  return axios.post(`${apiRoot}/users`, formData);
};
