import { apiRoot } from "./api";
import axios from "axios";
import { appendFormData } from "./config";
import { getAuthHeader } from "./authentication";

export const getPosts = () => {
  return axios.get(`${apiRoot}/posts`);
};

export const newPost = data => {
  console.log(data);
  let formData = appendFormData(data);
  console.log(formData);
  return axios.post(`${apiRoot}/posts`, formData, {
    headers: getAuthHeader()
  });
};

export const deletePost = postId => {
  return axios.delete(`${apiRoot}/posts/${postId}`, {
    headers: getAuthHeader()
  });
};
